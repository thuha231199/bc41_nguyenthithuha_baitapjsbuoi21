var DSNV = [];
//Lấy dữ liệu từ localStorage hi user load trang
let dataJson = localStorage.getItem("DSNV_LOCAL");
console.log("🚀 ~ file: main.js:4 ~ dataJson", dataJson)
//Kiểm tra nếu có data ở localStorage thì mới gán vào array DSNV
if (dataJson != null){
    var dataArr = JSON.parse(dataJson)
    DSNV = dataArr.map(function(item){
        var nv = new NhanVien(item.taiKhoan,
            item.hoTen,
            item.emailNV,
            item.matKhau,
            item.date,
            item.luongCB,
            item.chucVu,
            item.time,
            item.tinhLuong,
            item.xepLoaiNhanVien
            )
        return nv;
    })
}


//Thêm nhân viên
function themNV(){
    var nv = layThongTinTuForm();

    // validate ~ kiểm tra dữ liêu 
    var isValid = true;
    isValid = 
    kiemTraDoDai(nv.taiKhoan,"tbTKNV",4,6) &&
    kiemTraTrung(nv.taiKhoan, DSNV) && 
    kiemTraTen(nv.hoTen) &&
    kiemTraMatKhau(nv.matKhau) &&
    kiemTraLuong(nv.luongCB, "tbLuongCB",1000000,20000000) &&
    kiemTraGioLam(nv.time,"tbGiolam",80,200);
    isValid= isValid & kiemTraEmail(nv.emailNV);
    
    


    //push nv vào DSNV
    DSNV.push(nv);
    //convert array DSNV thành json
    var dsnvJson = JSON.stringify(DSNV);
    //lưu json vào localStorage
    localStorage.setItem("DSNV_LOCAL", dsnvJson);
    renderDSNV(DSNV);
}


//Xóa nhân viên
function xoaNV(idNV){
    //splice(viTri,1)
    var viTri = timKiemViTri(idNV,DSNV);
    if (viTri != -1){
        DSNV.splice(viTri,1);
        renderDSNV(DSNV);
    }
}


//Sửa nhân viên
function suaNV(idNV){
    var viTri = timKiemViTri(idNV,DSNV);
    if (viTri != -1){
        var nv = DSNV[viTri];
        document.getElementById("tknv").disabled=true;
    }
}