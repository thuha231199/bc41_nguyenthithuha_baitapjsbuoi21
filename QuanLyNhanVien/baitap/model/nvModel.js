function NhanVien(
    _taiKhoan,
    _hoTen,
    _emailNV,
    _matKhau,
    _ngayLam,
    _luongCoBan,
    _chucVu,
    _gioLam){
        this.taiKhoan = _taiKhoan;
        this.hoTen = _hoTen;
        this.emailNV = _emailNV;
        this.matKhau = _matKhau;
        this.date = _ngayLam;
        this.luongCB = _luongCoBan;
        this.chucVu = _chucVu;
        this.time = _gioLam;
        this.tinhLuong = this.tinhTongLuong(),
        this.xepLoaiNhanVien = this.xepLoai()
}

//Tính tổng lương
NhanVien.prototype.tinhTongLuong = function ()
    {
        if(this.chucVu == 'Sếp') {
            return this.luongCoBan*3
        } else if (this.chucVu == 'Trưởng Phòng') {
            return  this.luongCoBan*2
        } else {
            return this.luongCoBan
        }
    }
//Xếp loại
    NhanVien.prototype.xepLoai = function () {
        let ketQua = ''
        if (this.time >=192) {
            ketQua = 'Xuất Sắc'
        } else if (this.time >=176) {
            ketQua = 'Giỏi'
        } else if (this.time >=160) {
            ketQua = 'Khá'
        } else {
            ketQua= 'Trung Bình'
        }
        return ketQua;
}