function kiemTraTrung(idNV,nvArr){
    var viTri = nvArr.findIndex(function (item){
        return item.taiKhoan == idNV;
    });
    if (viTri!= -1){
        document.getElementById("tbTKNV").innerText = "Mã nhân viên đã tồn tại";
        return false;
    } else {
        document.getElementById("tbTKNV").innerText= "";
        return true;
    }

}
//Kiểm tra tối đa 4-6 số, không để trống
function kiemTraDoDai(value,idErr, min, max) {
    var length = value.length;
    if (length<min || length>max) {
        document.getElementById(idErr).innerText=`Độ dài phải từ ${min} đến ${max} kí tự`;
        return false;
    } else {
        document.getElementById(idErr).innerText ="";
        return true;
    }
}

//Kiểm tra email
function kiemTraEmail(value){
    const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var isEmail = re.test(value);
    if(isEmail){
        document.getElementById("tbEmail").innerText= "";
        return true;
    } else {
        document.getElementById("tbEmail").innerText= "Email không đúng định dạng";
        return false;
    }
}

//Kiểm tra họ tên
function kiemTraTen(value){
    const re = /^(([A-Za-z]+[\-\']?)*([A-Za-z]+)?\s)+([A-Za-z]+[\-\']?)*([A-Za-z]+)?$/;
    var isHoTen = re.test(value);
    if(isHoTen){
        document.getElementById("tbTen").innerText= "";
        return true;
    } else {
        document.getElementById("tbTen").innerText= "Vui lòng nhập tên là chữ cái";
        return false;
    }
}

//Kiểm tra mật khẩu
function kiemTraMatKhau(value){
    const re = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{6,10}$/;
    var isMatKhau = re.text(value);
    if(isMatKhau){
        document.getElementById("tbMatKhau").innerText="";
        return true;
    } else {
        document.getElementById("tbMatKhau").innerText="Vui lòng nhập ít nhất 1 chữ cái viết hoa, 1 ký tự số, 1 ký tự đặc biệt"
        return false;
    }
}


//Kiểm tra lương
function kiemTraLuong(value,idErr, min, max) {
    var length = value.length;
    if (length<min || length>max) {
        document.getElementById(idErr).innerText=`Lương phải từ ${min} đến ${max} `;
        return false;
    } else {
        document.getElementById(idErr).innerText ="";
        return true;
    }
}

//Kiểm tra giờ làm trong tháng
function kiemTraGioLam(value,idErr, min, max) {
    var length = value.length;
    if (length<min || length>max) {
        document.getElementById(idErr).innerText=`Giờ làm phải từ ${min}giờ đến ${max} giờ`;
        return false;
    } else {
        document.getElementById(idErr).innerText ="";
        return true;
    }
}