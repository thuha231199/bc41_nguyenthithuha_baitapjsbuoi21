function layThongTinTuForm(){
    //lấy thông tin từ form
    var _taiKhoan = document.getElementById("tknv").value;
    var _hoTen = document.getElementById("name").value;
    var _emailNV = document.getElementById("email").value;
    var _matKhau = document.getElementById("password").value;
    var _ngayLam = document.getElementById("datepicker").value;
    var _luongCoBan = document.getElementById("luongCB").value;
    var _chucVu = document.getElementById("chucvu").value;
    var _gioLam = document.getElementById("gioLam").value;
    //tạo object sv
    return new NhanVien (
        _taiKhoan,
        _hoTen,
        _emailNV,
        _matKhau,
        _ngayLam,
        _luongCoBan,
        _chucVu,
        _gioLam)
}

function renderDSNV(nvArr){
    //render danh sách nhân viên
    //contentHTML ~ chuỗi chứa cái thẻ tr
    var contentHTML="";
    for (var index = 0; index < nvArr.length; index ++) {
        var nv = nvArr[index];
        var contentTr =`<tr>
        <td>${nv.taiKhoan}</td>
        <td>${nv.hoTen}</td>
        <td>${nv.emailNV}</td>
        <td>${nv.date}</td> 
        <td>${nv.chucVu}</td>
        <td>${nv.tinhLuong}</td>
        <td>${nv.xepLoaiNhanVien}</td>
        <td>
        <button onclick="xoaNV('${nv.taiKhoan}')" class="btn btn-danger">
        Xóa</button>
        <button
        onclick="suaNV('${nv.taiKhoan}')"
        class="btn btn-warning">
        Sửa</button>
        </td>
        </tr>
        `;
        contentHTML = contentHTML + contentTr;
    }
    //show ra table
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
}


function timKiemViTri(id,arr){
    var viTri = -1;
    for ( var index = 0; index < arr.length; index++){
        if (arr[index].taiKhoan == id) {
            viTri = index;
        }
    }
    return viTri;
}